package com.conquers.sistercity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistercityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistercityApplication.class, args);
	}

}
